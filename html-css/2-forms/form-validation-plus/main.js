const email = document.getElementById('email');
const emailConfirm = document.getElementById('email-confirm');

const password = document.getElementById('password');
const passwordConfirm = document.getElementById('password-confirm');


const checkSame = (confirmA, confirmB, message) => {
    if (confirmA.value !== confirmB.value) {
        confirmB.setCustomValidity(message)
    } else confirmB.setCustomValidity('');
}

document.addEventListener('click', ({ target }) => {
    if (target.id === "submit-btn") {
        checkSame(email, emailConfirm, "Emails don't match.");
        checkSame(password, passwordConfirm, "Passwords don't match.");

    }

})


password.oninvalid = function (event) {
    event.target.setCustomValidity('Password must have at least one: uppercase, lowercase, number');
}
