# My Odin Projects  
[The Odin Project](https://www.theodinproject.com) is a webpage that gives self thaught developers the necessary tools to make their first steps in the web developing world.  



## FullStack JavaScript Path  
I chose the JavaScript path. The course is divided into 4 sections.
- Web Development 101
- ES6 JavaScript  
- Node/Express  
- HTML/CSS  
 
### Web Development 101 Projects   
- HTML/CSS  
    https://elfran45.gitlab.io/odin-google-main/  
- Rock Paper Scissors  
  https://elfran45.gitlab.io/odin-rps/  
- Etch a Sketch  
  https://elfran45.gitlab.io/odin-etch-a-sketch/  
- Calculator  
    https://elfran45.gitlab.io/calculator/  

### ES6+ JavaScript Projects  
- Library  
  Page: https://elfran45.gitlab.io/odin-book-library/  
  Repo: https://gitlab.com/elfran45/odin-boo-library  
- Tic Tac Toe  
  Page: https://elfran45.gitlab.io/tic-tac-toe/  
  Repo  https://gitlab.com/elfran45/tic-tac-toe  
- Restaurant Page  
  Page: https://elfran45.gitlab.io/restaurant-nordkkap/  
  Repo: https://gitlab.com/elfran45/restaurant-nordkkap  
- Todo List  
  Page: https://elfran45.gitlab.io/todo-list/  
  Repo: https://gitlab.com/elfran45/todo-list  
- FrameWork React  
  Page: 
  Repo: https://github.com/elfran45/planit 
- Weather App  
  Page: https://elfran45.gitlab.io/weather-app/  
  Repo: https://gitlab.com/elfran45/weather-app  
  

### HTML/CSS Projects  
- Embeding Video  
  Repo: https://gitlab.com/elfran45/odin-project/-/tree/master/html-css/1embedding-img-video
- Forms  
  Page: https://elfran45.gitlab.io/form-validation/    
  Repo: https://gitlab.com/elfran45/fomr-validation   
- Positioning  
  Repo: https://gitlab.com/elfran45/odin-project/-/tree/master/html-css/3-positioning
- Background - Gradients  
  Repo: https://gitlab.com/elfran45/odin-project/-/tree/master/html-css/4-backgrounds-gradients
- design Teardown  
  Repo: https://gitlab.com/elfran45/odin-project/-/tree/master/html-css/5-design-teardown
- Building responsive  
  Repo: https://gitlab.com/elfran45/odin-project/-/tree/master/html-css/6-building-responsive


### Node Projects  
**In this section I took a different approach outside the odin project, I applied the given concepts on my own full stack projects**  

## 1 - Web page containing World's countries data:  
### github Repo  
https://github.com/elfran45/express-countries  
### Deployment on Heroku  
https://worldcountries.herokuapp.com/  
![countriespc](/uploads/0970b5eb4f902194432ae28affbaf03d/countriespc.png)  
![phonecountries](/uploads/e08370d46d7f25d14bf5aedeabb2ef91/phonecountries.png)

## 2 - Todo App with multiple Lists: 
### github Repo  
https://gitlab.com/elfran45/todo-app  
![notebog](/uploads/f27686ffb53483289be879198bf55b6c/notebog.png)








