
const Element = () => {
    const renderElement = (elementType, className, text = "") => {
        const htmlElement = document.createElement(`${elementType}`);
        htmlElement.classList.add(`${className}`);
        htmlElement.innerText = `${text}`

        return htmlElement;
    };

    const renderImg = (url) => {
        const img = document.createElement('IMG');
        img.setAttribute("src", `${url}`);
        return img;
    }

    return { renderElement, renderImg };
}

const renderContent = () => {
    const content = document.querySelector(".content");

    const headerDiv = Element().renderElement("div", "header", "");
    const headerUl = Element().renderElement("ul", "header-ul", "");

    const home = Element().renderElement("li", "home", "Home");
    const menu = Element().renderElement("li", "menu", "Menu");
    const logo = Element().renderElement("li", "logo", "");
    const gallery = Element().renderElement("li", "gallery", "Gallery");
    const AboutUs = Element().renderElement("li", "about", "About Us");

    const logoImage = Element().renderElement("div", "logo-image", "");
    const nordkkap = Element().renderImg("./src/img/nordkkap-logo.png");
    logoImage.append(nordkkap);
    logo.append(logoImage);

    headerUl.append(home, menu, logo, gallery, AboutUs);
    headerDiv.append(headerUl);
    content.append(headerDiv);


    return content;
}

export { Element, renderContent };


