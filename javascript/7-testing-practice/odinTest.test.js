const odinTest = require('./odinTest');

test('capitalize a string', () => {
    expect(odinTest.capitalize('john')).toBe('John')
});

test('reverse a string', () => {
    expect(odinTest.reverseString('john')).toBe('nhoj')
});

test('reverse a string', () => {
    expect(odinTest.calculator.add([2,1])).toBe(3);
    expect(odinTest.calculator.substract([2,1])).toBe(1);
    expect(odinTest.calculator.divide([12,2])).toBe(6);
    expect(odinTest.calculator.multiply([2,2])).toBe(4);
});

test('return an object', () => {
    expect(odinTest.analyze([1,2,3,4,5])).toEqual({
        average: 3,
        min: 1,
        max: 5,
        length: 5
    });
});


