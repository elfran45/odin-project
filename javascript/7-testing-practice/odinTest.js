const odinTests = {
    capitalize: (string) => string.charAt(0).toUpperCase() + string.slice(1),
    reverseString: (string) => string.split('').reverse().join(''),
    calculator: {
        add: (array) => array.reduce((a,b) => a+b),
        substract: (array) => array.reduce((a,b) => a- b),
        divide: (array) => array.reduce((a,b) => a/b),
        multiply: (array) => array.reduce((a,b) => a*b)
    },
    analyze: (array) => {
        const sum = () => array.reduce((a,b) => a+b)
        return {
            average: sum()/array.length,
            min: Math.min(...array),
            max: Math.max(...array),
            length: array.length
        }
    }
}

module.exports = odinTests;