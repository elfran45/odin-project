const caesarCipher = require('./caesar');

test('must return an encoded message', () => {
    expect(caesarCipher('zzzz', 1)).toBe('aaaa');
    expect(caesarCipher('zZZZ', 1)).toBe('aaaa');
    expect(caesarCipher('holalocotodobiencomoandas', 2)).toBe('jqncnqeqvqfqdkgpeqoqcpfcu');
    expect(caesarCipher('zzzz', 3)).toBe('cccc');
    expect(caesarCipher('yosoypablozarate', 4)).toBe('cswsctefpsdevexi');
})
