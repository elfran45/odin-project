const caesarCipher = (message, shiftValue) => {
    const alphabetIndices = 'abcdefghijklmnopqrstuvwxyz'.split('');
    let messageArray = message.toLowerCase().split('');

    messageArray = messageArray.map(letter => alphabetIndices.indexOf(letter));

    messageArray = messageArray.map(number => {
        if(number+shiftValue > 26){return number = number+shiftValue-26} 
        else if(number+shiftValue < 26){return number = number+shiftValue}
        else return number = 0
    })

    messageArray = messageArray.map(number => {
        return number = alphabetIndices[number]
    })

    messageArray = messageArray.join('')

    return messageArray
}

module.exports = caesarCipher

