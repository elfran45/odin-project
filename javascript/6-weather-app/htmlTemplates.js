const currentDay = document.querySelector(".current-day");
const input = document.querySelector("#input");
const searchBtn = document.querySelector(".search-btn");
const languages = document.querySelector(".languages");

function toCelsius(temp){
    let tempCelsius = temp - 273;
    return tempCelsius.toFixed();
}

function toFahrenheit(temp){
    let tempFahrenheit = ((temp-273.15)*1.8)+32;
    return tempFahrenheit;
}

const dateObj = new Date();
const month = dateObj.toLocaleString('default', { month: 'long' });
const day = dateObj.getUTCDate();

const newdate = `${day} ${month}`;

let currentWeatherTemplate = function(weather){
    const location = document.createElement("div");
    const currentDate = document.createElement("div");
    const img = document.createElement("img");
    const currentWeatherDiv = document.createElement("div");
    const currentTemperatureDiv = document.createElement("div");
    const maxMinTemperatureDiv = document.createElement("div");
    currentWeatherDiv.classList.add("current-weather");
    currentTemperatureDiv.classList.add("temperature");
    location.classList.add("location");
    maxMinTemperatureDiv.classList.add("max-min-weather");
    
    location.innerText = weather.name;
    currentDate.innerText = newdate;
    img.src = weather.iconWeatherURL;
    currentWeatherDiv.innerText = weather.currentWeather;
    currentTemperatureDiv.innerText = `${toCelsius(weather.avgTemp)} C°`;
    maxMinTemperatureDiv.innerText = `Min ${toCelsius(weather.minTemp)} C° - Max ${toCelsius(weather.maxTemp)} C°`;
    currentDay.append(location, currentDate,img,currentWeatherDiv, currentTemperatureDiv, maxMinTemperatureDiv);
    return currentDay;
};



export {toCelsius, toFahrenheit, currentWeatherTemplate};