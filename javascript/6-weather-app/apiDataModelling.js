function getWeatherPromise(location){
    return fetch(`https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=b914ad6ab33e39d961fa4addb837863c&lang=sp`, {mode: 'cors'})
    .then(promise => promise.json());
};

let weatherInfo = function(location){
    let weatherInfo = getWeatherPromise(location);
    return weatherInfo.then(weather=>{
        console.log(weather)
        return {
            name: `${weather.name} (${weather.sys.country})`,
            avgTemp : weather.main.temp,
            minTemp : weather.main.temp_min,
            maxTemp : weather.main.temp_max,
            currentWeather: weather.weather[0].main,
            iconWeatherURL: `https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`
        }
    })
};

export {getWeatherPromise, weatherInfo};