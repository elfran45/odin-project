const Board = (() => {
    let valuesPlayed = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
    let turn = false;
    let render = false;
    const board = document.querySelector('.board');

    const renderBoard = () => {
        if (render === false) {
            for (let i = 0; i < 9; i++) {
                let id = i;
                const tile = document.createElement('div');
                tile.classList.add('tile');
                tile.id = id;
                valuesPlayed[i] = tile.innerText;
                board.appendChild(tile);
                render = true;
            }
        } else return;
    };

    const clearBoard = () => {
        board.innerHTML = "";
        Board.valuesPlayed = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
        render = false;
        renderBoard();

    };
    const getValuesPlayed = (x) => {
        return valuesPlayed[x];
    };
    document.addEventListener('click', ({ target }) => {
        if (target.matches('.reset-button')) {
            clearBoard();

        }
    });
    document.addEventListener('click', ({ target }) => {
        if (target.matches('.start-button')) {
            Board.valuesPlayed = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
            renderBoard();
            Game.updateScore();


        }
    });
    return { getValuesPlayed, turn, valuesPlayed, clearBoard };
})();

const Player = (name, type) => {
    let score = 0
    const getName = () => name;
    const getType = () => type;
    const addScore = () => score++;
    const getScore = () => score;

    return { getName, getType, addScore, getScore }
}

document.addEventListener('click', ({ target }) => {
    const selectTile = (playerType) => {
        Board.valuesPlayed[target.id] = playerType;
        target.textContent = playerType;
    }
    if (target.matches('.tile') && Board.turn === false && target.textContent === "") {
        selectTile("X");
        Board.turn = true;
        Game.gameLogic();
    } else if (target.matches('.tile') && Board.turn === true && target.textContent === "") {
        selectTile("O");
        Board.turn = false;
        Game.gameLogic();
    }

})

const Game = (() => {
    let x;
    const whoWon = () => {
        if (Board.valuesPlayed[x] === "X") {
            console.log('Gano X');
            player1.addScore();
        } else if (Board.valuesPlayed[x] === "O") {
            console.log('Gano O');
            player2.addScore();
        }
    }
    const updateScore = () => {
        const xScore = document.querySelector(".x-score");
        xScore.innerText = `${player1.getName()} X: ${player1.getScore()}`;
        const oScore = document.querySelector(".o-score");
        oScore.innerText = `${player2.getName()} O: ${player2.getScore()}`;
    }

    const gameLogic = () => {

        for (x = 0; x < 9; x++) {
            switch (x) {
                case 0: if (Board.valuesPlayed[x] === Board.valuesPlayed[x + 1] && Board.valuesPlayed[x] === Board.valuesPlayed[x + 2]) {
                    whoWon();
                    updateScore();
                    Board.clearBoard();
                } else if (Board.valuesPlayed[x] === Board.valuesPlayed[x + 3] && Board.valuesPlayed[x] === Board.valuesPlayed[x + 6]) {
                    whoWon();
                    updateScore();
                    Board.clearBoard();
                } else if (Board.valuesPlayed[x] === Board.valuesPlayed[x + 4] && Board.valuesPlayed[x] === Board.valuesPlayed[x + 8]) {
                    whoWon();
                    updateScore();
                    Board.clearBoard();

                } else if (Board.valuesPlayed[x] === Board.valuesPlayed[x + 3] && Board.valuesPlayed[x] === Board.valuesPlayed[x + 6]) {
                    whoWon();
                    updateScore();

                } else if (Board.valuesPlayed[x] === Board.valuesPlayed[x + 4] && Board.valuesPlayed[x] === Board.valuesPlayed[x + 8]) {
                    whoWon();
                    updateScore();

                } break;
                case 3:
                case 6:
                    if (Board.valuesPlayed[x] === Board.valuesPlayed[x + 1] && Board.valuesPlayed[x] === Board.valuesPlayed[x + 2]) {
                        whoWon();
                        updateScore();
                        Board.clearBoard();

                    } break;

                case 1:
                case 2:
                    if (Board.valuesPlayed[x] === Board.valuesPlayed[x + 3] && Board.valuesPlayed[x] === Board.valuesPlayed[x + 6]) {
                        whoWon();
                        updateScore();
                        Board.clearBoard();
                    } else if (Board.valuesPlayed[x] === Board.valuesPlayed[x + 2] && Board.valuesPlayed[x] === Board.valuesPlayed[x + 4]) {
                        whoWon();
                        updateScore();
                        Board.clearBoard();

                    } else if (Board.valuesPlayed[x] === Board.valuesPlayed[x + 2] && Board.valuesPlayed[x] === Board.valuesPlayed[x + 4]) {
                        whoWon();
                        updateScore();
                    }
                    break;
            }
        }
    }
    return { gameLogic, updateScore };
})();

const player1 = Player(prompt("player 1 name"), "X");
const player2 = Player(prompt("player 2 name"), "O");
