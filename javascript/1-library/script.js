const library = document.querySelector('.library');
const newBookButton = document.querySelector('.new-book-btn');
const bookForm = document.querySelector('.book-form');


let myLibrary = [];

window.onload = () => {
    document.querySelector(".book-form").style.display = "none";
}

function toggleForm() {
    if (bookForm.style.display === "none") {
        bookForm.style.display = "flex";
    } else {
        bookForm.style.display = "none";
    }
}

newBookButton.addEventListener('click', toggleForm);

const addBook = () => {
    let newBook = {
        title: document.getElementById('title').value,
        author: document.getElementById('author').value,
        pages: document.getElementById('pages').value,
        read: false,
    };
    myLibrary.push(newBook);
    document.forms[0].reset();
}

function renderMyLibrary() {
    library.innerHTML = '';

    for (let i = 0; i < myLibrary.length; i++) {
        let id = i;
        const book = document.createElement('div');
        book.classList.add('book');
        book.id = id;
        const readDiv = document.createElement('div');
        readDiv.classList.add('div-read-button');
        const read = document.createElement('BUTTON');
        read.classList.add('read-button');

        if (myLibrary[i].read === false) {
            read.textContent = 'Not Read';
        } else { read.textContent = 'Read'; }


        read.dataset.for = id;
        const newTitle = document.createElement('div');
        newTitle.classList.add("title");
        newTitle.textContent = `${myLibrary[i].title}`;
        const newAuthor = document.createElement('div');
        newAuthor.classList.add("author")
        newAuthor.textContent = `${myLibrary[i].author}`;
        const newPages = document.createElement('div');
        newPages.classList.add("pages");
        newPages.textContent = `${myLibrary[i].pages}`;
        const deleteButtonDiv = document.createElement('div');
        deleteButtonDiv.classList.add('div-delete-button');
        const deleteButton = document.createElement('BUTTON');
        deleteButton.classList.add('delete-button');
        deleteButton.textContent = `Delete`;
        deleteButton.dataset.for = id;
        readDiv.append(read);
        book.append(readDiv);
        book.append(newTitle);
        book.append(newAuthor);
        book.append(newPages);
        deleteButtonDiv.append(deleteButton);
        book.append(deleteButtonDiv);
        library.append(book);
    }
}

document.getElementById('submit').addEventListener('click', () => {
    if (document.getElementById('title').value !== "" &&
        document.getElementById('author').value !== "" && document.getElementById('pages').value !== "") {
        addBook();
        renderMyLibrary();
    }




});


document.addEventListener('click', ({ target }) => {
    if (target.matches('.delete-button')) {
        myLibrary.splice(target.dataset.for, 1);
        renderMyLibrary();

    }
})

document.addEventListener('click', ({ target }) => {
    if (target.matches('.read-button') && target.textContent === 'Not Read') {
        target.textContent = 'Read';
        myLibrary[target.dataset.for].read = true;

    } else if (target.matches('.read-button') && target.textContent === 'Read') {
        target.textContent = 'Not Read';
        myLibrary[target.dataset.for].read = false;
    }
})







