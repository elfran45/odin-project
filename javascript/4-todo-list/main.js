"use strict";

const container = document.querySelector(".container");
const newLineBtn = document.getElementById("new-line");

const List = function () {
    let list = [];

    const createLineObject = function () {

        let lineObject = {
            value: document.getElementById('line').value,
            check: false
        };
        if (lineObject.value !== "") {
            list.push(lineObject);
            document.forms[0].reset();
        } else { return; }
    };
    return { list, createLineObject };
};

const renderLine = function () {
    container.innerHTML = "";
    for (let i = 0; i < list1.list.length; i += 1) {

        let id = i;
        const line = document.createElement('div');
        const textCheckWrapper = document.createElement('div');
        const lineCheck = document.createElement('div');
        const lineText = document.createElement('div');
        const buttons = document.createElement('div');
        const editBtnDiv = document.createElement('div')
        const editBtn = document.createElement('BUTTON');
        const delBtnDiv = document.createElement('div')
        const delBtn = document.createElement('BUTTON');

        textCheckWrapper.classList.add('text-check-wrapper');

        line.classList.add('line');
        line.id = id;
        lineCheck.classList.add('line-check');
        lineCheck.dataset.for = id;
        lineText.classList.add('line-text');

        if (list1.list[i].check === false) {
            lineCheck.textContent = '☒';
        } else {
            lineCheck.textContent = '☑';
            lineText.classList.add('crossed-line');
        }

        lineText.innerText = `${list1.list[i].value}`;

        buttons.classList.add('buttons');
        delBtn.classList.add('del-btn');
        editBtn.classList.add('edit-btn');

        editBtn.dataset.for, delBtn.dataset.for = id;
        editBtn.innerHTML = `<img id="${id}" class="edit-btn-img" src="./img/edit.png" width=15/>`;
        delBtn.innerHTML = `<img id="${id}" class="del-btn-img" src="./img/trash.png" width=15/>`;

        editBtnDiv.append(editBtn);
        delBtnDiv.append(delBtn);
        buttons.append(editBtnDiv, delBtnDiv);
        textCheckWrapper.append(lineCheck, lineText);
        line.append(textCheckWrapper, buttons);
        container.append(line);
    }
}

const checkLine = (selectedLine) => {
    if (selectedLine.textContent === '☒') {
        selectedLine.textContent = '☑';
        list1.list[selectedLine.dataset.for].check = true;

    } else if (selectedLine.textContent === '☑') {
        selectedLine.textContent = '☒';
        list1.list[selectedLine.dataset.for].check = false;
    }
}

const toggleCrossedLine = (selectedLine) => {
    selectedLine.nextSibling.classList.toggle("crossed-line");
}

const deleteLine = (deletedLine) => {
    list1.list.splice(deletedLine.id, 1);
}

const editLine = (selectedLine) => {
    list1.list[selectedLine.id].value = prompt('Modify Line');
}


document.addEventListener('click', ({ target }) => {
    if (target.matches('.edit-btn-img')) {
        editLine(target);
        renderLine();
    }
})

document.addEventListener('click', ({ target }) => {
    if (target.matches('.del-btn-img')) {
        deleteLine(target);
        renderLine();
    } else window.addEventListener('keydown', function (e) {
        if (e.keyIdentifier == 'U+000A' || e.keyIdentifier === 'Enter' ||
            e.keyCode === 13) { if (e.target.nodeName === 'INPUT' && e.target.type === 'text') { e.preventDefault(); return false; } }
    }, true)

})

newLineBtn.addEventListener('click', () => {
    list1.createLineObject();
    renderLine();
})

document.addEventListener('click', ({ target }) => {
    if (target.matches('.line-check')) {
        checkLine(target);
        toggleCrossedLine(target);
    }
})

const list1 = List();