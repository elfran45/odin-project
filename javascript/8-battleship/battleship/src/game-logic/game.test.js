const game = require('./game');
const createShip = require('./ship');



test('Check absolute positioning', () => {
    const fleet = game.createFleet('player');
    const ship = createShip(2,2,2,true);
    const ship2 = createShip(3,2,2,true);
    const ship3 = createShip(6,2,2,true);
    fleet.pushShip(ship);
    fleet.pushShip(ship2);
    fleet.pushShip(ship3);
    
    
    expect(fleet.absolutePosition).toEqual(['[2,2]', '[3,2]','[6,2]', '[7,2]']);
    expect(fleet.pushShip(ship2)).toBe('Another ship is there');

    
})
