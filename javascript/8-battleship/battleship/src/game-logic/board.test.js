
const createBoard = require('./board');


test('board gives a 2d array', () => {
    const board = createBoard();
    expect(board[0][0]).toBe('.');
    expect(board[5][4]).toBe('.');
    expect(board[9][9]).toBe('.');
});
