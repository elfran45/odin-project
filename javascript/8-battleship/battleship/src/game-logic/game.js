const createBoard = require('./board')

const playerBoard = createBoard();
const pcBoard = createBoard();

const createFleet = (fleetName) => {
    return ({
        fleetName,
        turn: false,
        absolutePosition: [],
        ships: [],
        checkCollisions: function(newShip){
            return this.absolutePosition.some(slot => newShip.positions.includes(slot));
            
        },
        pushShip: function(newShip){
            if(this.ships.length === 5) return 'No more ships available'
            if(this.checkCollisions(newShip)) return 'Another ship is there'
            else if(!this.checkCollisions(newShip)){
                newShip.positions.map(position => {
                    this.absolutePosition.push(position);
                })
                this.ships.push(newShip)
            } 
        }
    })
}

const populateBoard = (board, fleet) => {
    fleet.absolutePosition.forEach(position => {
        const coordinates = JSON.parse(position);
        const x = coordinates[0];
        const y = coordinates[1];
        board[x][y] = 'S';
    })
}

const fireCannonsAt = (board,x,y) => {
    if(board[x][y] === '.'){
        return 'You missed'
    } else if(board[x][y] === 'S'){
        board[x][y] = 'H'
        return 'Hit!'
    } else{
        return 'You already fired at that place'
    }
}

// Game Signs: 
// water === .
// ship === S
// miss === M
// hit = H


// bueno genial ahora que me falta ?
// ya tengo la creacion de barcos, del board pero me falta sumarle cosas a game
// que me falta en game ? me faltan los turnos, las condiciones de ganar



exports.createFleet = createFleet;
exports.populateBoard = populateBoard;
exports.fireCannonsAt = fireCannonsAt;