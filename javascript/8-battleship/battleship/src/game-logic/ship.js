
const createShip = (startPosX, startPosY, size, vertical = true) => {
    if(size>5 || size<=0) return;

    else {
        const positions = []
        if(vertical){
            for(let x = 0; x<size; x++){
                positions.push(JSON.stringify([startPosX + x, startPosY]))
            }
        } else if(vertical === false){
            for(let y = 0; y<size; y++){
                positions.push(JSON.stringify([startPosX, startPosY + y]))
            }
        }
        return (
            {
            size,
            damage: 0,
            shipHead: [startPosX, startPosY],
            positions: positions,
            horizontal: vertical,
            isSunk: function() {
                if(this.damage === this.size) return true
                else return false
            },
            hit: function() {
                if(this.isSunk()) return
                this.damage++
            } 
        }
        )
    }
}

module.exports = createShip;



