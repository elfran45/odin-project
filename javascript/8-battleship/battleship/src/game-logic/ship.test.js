const createShip = require('./ship');


test('Ship is sunk', () => {
    const ship = createShip(0,0,2,true);
        for(let i = 0; i<5;i++){
            ship.hit()
        }
    expect(ship.isSunk()).toBeTruthy()
});

test('Ship position horizontal and vertical', () => {
    const ship = createShip(0,0,2,true);
    const ship2 = createShip(0,0,2,false);
    expect(ship.positions).toEqual(["[0,0]","[1,0]"])
    expect(ship2.positions).toEqual(["[0,0]","[0,1]"])
    
});
