import React, { Component } from 'react';
import './ShipSelection.css'

export default class ShipSelection extends Component {
    render() {
        return (
            <div className="ship-selection">
                <div id="2" className="two">
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                </div>
                <div id="3" className="three">
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                </div>
                <div id="4" className="four">
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                </div>
                <div id="5" className="five">
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                    <div className="ship-tile"></div>
                </div>
            </div>
        )
    }
}

