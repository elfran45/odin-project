import React, { Component } from 'react';
import './Board.css'
import Tile from './Tile';
import board from '../game-logic/board';


export default class Board extends Component {

    constructor(props){
        super(props)
        this.state = {}
    }

    componentDidMount(){
        this.setState({
            visibleBoard: board()
        })
    }

    render() {
        let render = undefined;
        if(this.state.visibleBoard){
            if(this.props.visible){
                render = this.props.board.map((column, columnIndex) => {
                    return column.map( (tile, rowIndex) => {
                        return <Tile key={rowIndex}
                         x={columnIndex} 
                         y={rowIndex} 
                         tile={tile} 
                         pushShip={this.props.pushShip}
                         visible={this.props.visible} 
                         playerBoard={this.props.playerBoard}
                         vertical={this.props.vertical}/>
                    })
                })

            } else{
                render = this.state.visibleBoard.map((column, columnIndex) => {
                    return column.map( (tile, rowIndex) => {
                        return <Tile key={rowIndex}
                         x={columnIndex} 
                         y={rowIndex} 
                         tile={tile} 
                         fireCannonsAt={this.props.fireCannonsAt} 
                         pushShip={this.props.pushShip}
                         visible={this.props.visible} 
                         playerBoard={this.props.playerBoard}/>
                    })
                })
            }
        }
        
        return (
            <div className="grid">
                {render}
            </div>
        )
    }
}
