import React, { Component } from 'react';
import './Game.css'
import board from '../game-logic/board';
import game from '../game-logic/game';
import ship from '../game-logic/ship';
import Board from './Board';
import ShipSelection from './ShipSelection'

export default class Game extends Component {

    constructor(){
        super()
        this.state = {
            vertical: true,
            increment: 0,
            selectedSize: 2,
            board: undefined,
            enemyBoard: [[undefined]]
        }
    }

    componentDidMount(){
        this.setState({
            board: board(),
            enemyBoard: board(),
            fleet: game.createFleet('fran'),
            enemyFleet: game.createFleet('enemy')
        })
    }

    pushShip = (x,y,vertical) => {
        let increment = this.state.increment
        const sizes = [2,2,3,4,5]
        const updatedFleet = this.state.fleet
        const updatedBoard = this.state.board
        if(!this.state.vertical){
            if(y + sizes[increment] -1 > 9) return
        }
        if(this.state.vertical){
            if(x + sizes[increment] -1 > 9 ) return
        }
        updatedFleet.pushShip(ship(x,y,sizes[increment], vertical));
        game.populateBoard(updatedBoard, updatedFleet)
        increment++
        this.setState({increment: increment, board: updatedBoard,fleet: updatedFleet});
    }

    fireCannonsAt = (x,y) => {
        const updateState = this.state.enemyBoard
        if(this.state.enemyBoard[x][y] === '.'){
            updateState[x][y] = 'M'
            this.setState({enemyBoard: updateState})
            return this.state.enemyBoard[x][y]
            
        } else if(this.state.enemyBoard[x][y] === 'S'){
            
            updateState[x][y] = 'X'
            this.setState({enemyBoard: updateState})
            return this.state.enemyBoard[x][y]
        }
        else{   return 'You already fired at that place'}
    }

    handleDirection = () => {
        const vertical = this.state.vertical
        this.setState({vertical: !vertical})
    }

    render() {
        let render;
        if(this.state.enemyFleet)
        
        render = <div className="game">
            <Board board={this.state.board} visible={true} playerBoard={true} pushShip={this.pushShip} vertical={this.state.vertical}/>
            <Board board={this.state.enemyBoard} visible={false} fireCannonsAt={this.fireCannonsAt} playerBoard={false} />
        </div>
        return (
            <div>
                {render}
                <button onClick={this.handleDirection} ></button>
            </div>
        )
    }
}
