import React, { Component } from 'react'
import './Tile.css'

export default class Tile extends Component {

    constructor(props){
        super(props);
        this.state = {
            visible: false
        }
    }

    componentDidMount(){
        this.setState({visible: this.props.visible});
    }

    clickHandler = async () => {
        if(this.props.playerBoard){
            this.props.pushShip(this.props.x,this.props.y,this.props.vertical);
        } else{
            await this.props.fireCannonsAt(this.props.x, this.props.y);
            this.setState({visible: true});
        }
    }
    
    render() {
        let render = undefined;
        if(this.state.visible){
            render = <div className="tile" onClick={this.clickHandler} >{this.props.tile}</div>
        } else {
            render = <div className="tile" onClick={this.clickHandler} >{}</div>
        }

        return (
            <div className="tile-wrapper">
                {render}
            </div>
        )
    }
}
