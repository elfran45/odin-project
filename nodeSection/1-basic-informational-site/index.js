const express = require('express');
const fs = require('fs');
const url = require('url');
const app = express();

const renderHTML = (req,res) => {
    const parseUrl = url.parse(req.url, true);
    const templatePath = `${__dirname}/templates${parseUrl.pathname}.html`;
    if(!templatePath){
        return
    }
    res.sendFile(templatePath);
}

app.get('/index', renderHTML);
app.get('/about', renderHTML);
app.get('/contact-me', renderHTML);
app.get('/index', renderHTML);

app.all('*', (req, res, next) => {
	res.status(404).sendFile(`${__dirname}/templates/404.html`);
})

app.listen(5000, function(){
    console.log('listening on port 5000');
})