class SlideShow {
    constructor(selectDiv){
        this._images = [];
        this._imageIndex = 0;
        this.selectDiv = selectDiv;
    }
    pushImage(url){
        this._images.push(url);
    }
    renderSlideShow(imageIndex){
        const img = document.querySelector(this.selectDiv);
        img.src = this._images[imageIndex];
    }
    slideNext(){
        if(this._imageIndex === this._images.length - 1){
            this._imageIndex = 0;
            this.renderSlideShow(this._imageIndex);
        } else {
            this._imageIndex++;
            this.renderSlideShow(this._imageIndex);
        }
    }
    slidePrevious(){
        if(this._imageIndex === 0){
            this._imageIndex = this._images.length - 1;
            this.renderSlideShow(this._imageIndex)
        } else {
            this._imageIndex--;
            this.renderSlideShow(this._imageIndex);
        }
    }
    start(){
        this.renderSlideShow(this._imageIndex);
    }
}

let slideShow = new SlideShow(".slide-show-img");

slideShow.pushImage("https://shop.fender.com/on/demandware.static/-/Library-Sites-Fender-Shared/default/dwe891e220/fender/hp-2020/70th_Anniversary_inlt_desktop.jpg");
slideShow.pushImage("https://stuff.fendergarage.com/images/T/P/w/Web_Elec_0225_20_American_Original_Landing_Page_desktop_.jpg");
slideShow.pushImage("https://stuff.fendergarage.com/images/I/a/5/LEAD_II_III_HPS_DESKTOP_2x.jpg");
slideShow.pushImage("https://stuff.fendergarage.com/images/U/S/i/Web_Elec_0421_20_Jim_Root_Jazzmaster_HPS_Desktop.jpg");
slideShow.pushImage("https://shop.fender.com/on/demandware.static/-/Library-Sites-Fender-Shared/default/dw63ffbcaa/fender/hp-2020/0331_TomMorello_HPS.jpg");

slideShow.start();

const previousBtn = document.querySelector(".previous-btn");
const nextBtn = document.querySelector(".next-btn");

previousBtn.addEventListener('click', ()=>{
    slideShow.slidePrevious();
})
nextBtn.addEventListener('click', ()=>{
    slideShow.slidePrevious();
})
