let values = ["rock", "paper", "scissors"];
let playerPlay = null;
let computerPlay = null;
let playerScore = 0;
let computerScore = 0;

function rand() {
  let array = Math.floor(Math.random() * (3 - 1 + 1)) + 0;
  computerPlay = values[array];
  return computerPlay;
}

function gameLogic() {
  if (playerPlay === computerPlay) {
    return "Tie";
  } else if (playerPlay === "rock" && computerPlay === "paper") {
    computerScore++;
    return "Paper beats rock!";
  } else if (playerPlay === "rock" && computerPlay === "scissors") {
    playerScore++;
    return "Rock beats scissors!";
  } else if (playerPlay === "scissors" && computerPlay === "rock") {
    computerScore++;
    return "Rock beats scissors!";
  } else if (playerPlay === "scissors" && computerPlay === "paper") {
    playerScore++;
    return "Scissors beats paper!";
  } else if (playerPlay === "paper" && computerPlay === "rock") {
    playerScore++;
    return "Paper beats rock";
  } else if (playerPlay === "paper" && computerPlay === "scissors") {
    computerScore++;
    return "Scissors beats paper!";
  }
}



let btnRock = document.querySelector(".btnRock");
btnRock.addEventListener("click", () => {
  playerPlay = "rock";
});

let btnPaper = document.querySelector(".btnPaper");
btnPaper.addEventListener("click", () => {
  playerPlay = "paper";
});

let btnScissors = document.querySelector(".btnScissors");
btnScissors.addEventListener("click", () => {
  playerPlay = "scissors";
});

let outcome = document.querySelector(".outcome");
let results = function () {
  outcome.textContent = `Values: ${gameLogic()}`;
};

let infoLog = function () {
  let pScore = document.querySelector(".pScore");
  pScore.textContent = `Player Score: ${playerScore}`;

  let cScore = document.querySelector(".cScore");
  cScore.textContent = `Computer Score: ${computerScore}`;
};

let btnPlay = document.querySelector(".btnPlay");
btnPlay.addEventListener("click", () => {
  rand();
  results();
  infoLog();
});

let btnReset = document.querySelector(".btnReset");
btnReset.addEventListener("click", () => {
  playerScore = 0;
  computerScore = 0;
  playerPlay = null;
  computerPlay = null;
  infoLog();
  outcome.textContent = `Tie`;
});